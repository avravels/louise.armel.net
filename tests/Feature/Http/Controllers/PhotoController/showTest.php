<?php

declare(strict_types=1);

it('requires a password', function (): void {
    $response = get('/');

    expect($response)
        ->getStatusCode()
        ->toBe(401)
        ->and($response)
        ->getHeader('WWW-Authenticate')
        ->toContain('Basic realm="Login"');
});

it('returns 200 with correct authorization', function (): void {
    $response = get('/', [
        'Authorization' => sprintf('Basic %s', base64_encode(sprintf('%s:%s', app('username'), app('password')))),
    ]);

    expect($response)
        ->getStatusCode()
        ->toBe(200);
});

it('returns 401 with incorrect authorization', function (string $credentials): void {
    $response = get('/', [
        'Authorization' => sprintf('Basic %s', base64_encode($credentials)),
    ]);

    expect($response)
        ->getStatusCode()
        ->toBe(401)
        ->and($response)
        ->getHeader('WWW-Authenticate')
        ->toContain('Basic realm="Login"');
})->with(function (): Generator {
    yield 'failure:failure';
    yield 'failure:';
    yield ':admin';
    yield 'admin';
    yield 'admin:';
    yield '';
    yield ':';
});
