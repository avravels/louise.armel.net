<?php

declare(strict_types=1);

use App\Http\Kernel;

require_once __DIR__ . '/../vendor/autoload.php';

app(Kernel::class)->run();
