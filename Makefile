EXEC="docker compose exec php"

up:
	docker compose up -d

start: up composer-install npm-install tailwind-build

composer-install:
	"$(EXEC)" composer install

stop:
	docker compose stop

npm-install:
	"$(EXEC)" npm install

tailwind-watch:
	"$(EXEC)" npx tailwindcss -i ./resources/css/main.css -o ./resources/assets/css/main.css --watch

tailwind-build:
	"$(EXEC)" npx tailwindcss -o ./resources/assets/css/main.css --minify

cs-fix:
	"$(EXEC)" vendor/bin/php-cs-fixer fix --allow-risky=yes

phpstan:
	"$(EXEC)" vendor/bin/phpstan analyse src --level 8

php-sh:
	"$(EXEC)" ./jungaa utility:shell
