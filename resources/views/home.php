<?php /** @var \League\Flysystem\FileAttributes[] $photos */ ?>
<?php /** @var \App\Filesystem\Factories\FileAttributesUrlFactory $urlFactory */ ?>

<?php $this->layout('template', ['title' => app('app-name')]); ?>
<div class="gap-0 columns-3 sm:columns-4 md:columns-6 lg:columns-8 xl:columns-10 2xl:columns-12">
<?php foreach($photos as $photo): ?>
    <a class="block p-2" href="<?= $urlFactory->create($photo, \App\Enums\FilesystemFolder::Photos) ?>"><img class="transition duration-150 hover:brightness-100 brightness-50" src="<?= $urlFactory->create($photo, \App\Enums\FilesystemFolder::Thumbnails) ?>" alt=""></a>
<?php endforeach; ?>
</div>
