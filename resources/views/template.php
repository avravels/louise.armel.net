<?php

use App\Enums\FilesystemFolder;
use App\Filesystem\Factories\FileAttributesUrlFactory;
use League\Flysystem\FileAttributes;
use League\Flysystem\StorageAttributes;

?>
<!doctype html>
<html>
<head>
    <title><?=$this->e($title)?></title>
    <link rel="stylesheet" href="/<?= app(FileAttributesUrlFactory::class)->create(FileAttributes::fromArray([StorageAttributes::ATTRIBUTE_PATH => 'css/main.css']), FilesystemFolder::Css) ?>">
</head>
<body class="bg-black">
<?=$this->section('content')?>
</body>
</html>
