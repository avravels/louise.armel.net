<?php /** @var int $status */ ?>
<?php $this->layout('template', ['title' => app('app-name')]); ?>
<div class="m-4 text-white text-2xl text-center">HTTP <?= $status ?> <?= $message ?></div>
