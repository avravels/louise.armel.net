<?php

declare(strict_types=1);

use App\Authentication\UserAuthenticator;
use App\Core\EnvironmentLoader;
use App\Crypt\Hasher;
use App\Enums\Environment;
use App\Filesystem\Factories\FilesystemAdapterFactory;
use App\Filesystem\Factories\PublicUrlGeneratorFactory;
use App\Filesystem\Factories\TemporaryUrlGeneratorFactory;
use App\Repository\PhotoRepository;
use App\Repository\PhotoRepositoryInterface;
use Aws\S3\S3Client;
use Intervention\Image\ImageManager;
use Laminas\Diactoros\Response;
use Laminas\Diactoros\ServerRequest;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use League\Flysystem\AwsS3V3\AwsS3V3Adapter;
use League\Flysystem\Filesystem;
use League\Flysystem\Local\LocalFilesystemAdapter;
use League\Plates\Engine;
use Psr\Container\ContainerInterface;

return [
    'app-name' => static fn () => $_ENV['APP_NAME'],
    'environment' => static fn (): Environment => Environment::from($_ENV['ENVIRONMENT']),
    'request' => static fn (): ServerRequest => ServerRequestFactory::fromGlobals($_SERVER, $_GET, $_POST, $_COOKIE, $_FILES),
    'response' => static fn (): Response => new Response(),
    'emitter' => static fn (): SapiEmitter => new SapiEmitter(),
    'base-dir' => dirname(__DIR__),
    'assets-dir' => dirname(__DIR__) . '/resources/assets/',
    'view-dir' => dirname(__DIR__) . '/resources/views',
    Hasher::class => static fn () => new Hasher($_ENV['APP_KEY']),
    's3-endpoint' => static fn () => $_ENV['S3_ENDPOINT'],
    's3-key' => static fn () => $_ENV['S3_KEY'],
    's3-secret' => static fn () => $_ENV['S3_SECRET'],
    's3-region' => static fn () => $_ENV['S3_REGION'],
    's3-bucket' => static fn () => $_ENV['S3_BUCKET'],
    LocalFilesystemAdapter::class => static fn (ContainerInterface $container) => new LocalFilesystemAdapter($container->get('assets-dir')),
    AwsS3V3Adapter::class => static fn (ContainerInterface $container) => new AwsS3V3Adapter($container->get(S3Client::class), $container->get('s3-bucket')),
    S3Client::class => static fn (ContainerInterface $container) => new S3Client([
        'endpoint' => $container->get('s3-endpoint'),
        'credentials' => [
            'key' => $container->get('s3-key'),
            'secret' => $container->get('s3-secret'),
        ],
        'region' => $container->get('s3-bucket'),
    ]),
    EnvironmentLoader::class => static fn (ContainerInterface $container) => new EnvironmentLoader($container->get('base-dir')),
    Filesystem::class => static fn (ContainerInterface $container) => new Filesystem(
        $container->get(FilesystemAdapterFactory::class)->create(),
        publicUrlGenerator: $container->get(PublicUrlGeneratorFactory::class)->create(),
        temporaryUrlGenerator: $container->get(TemporaryUrlGeneratorFactory::class)->create(),
    ),
    UserAuthenticator::class => static fn (ContainerInterface $container) => new UserAuthenticator(
        $container->get('username'),
        $container->get('password'),
    ),
    'username' => static fn (): string => $_ENV['USERNAME'],
    'password' => static fn (): string => $_ENV['PASSWORD'],
    ImageManager::class => static fn () => new ImageManager(['driver' => 'imagick']),
    PhotoRepositoryInterface::class => static fn (ContainerInterface $container) => $container->get(PhotoRepository::class),
    Engine::class => static fn (ContainerInterface $container): Engine => new Engine($container->get('view-dir')),
];
