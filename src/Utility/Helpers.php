<?php

declare(strict_types=1);

use App\Core\Container;
use App\Http\Response\View;
use League\Plates\Engine;
use Psr\Http\Message\ResponseInterface;

if (!function_exists('app')) { //@codeCoverageIgnore
    function app(?string $className = null): mixed
    {
        return null === $className ? Container::getInstance() : Container::getInstance()->get($className);
    }
}

if (!function_exists('response')) { //@codeCoverageIgnore
    function response(): ResponseInterface
    {
        return app('response');
    }
}

if (!function_exists('view')) { //@codeCoverageIgnore
    /**
     * @param mixed[] $args
     */
    function view(string $template = '', array $args = []): View
    {
        /** @var View $response */
        $response = app(View::class);

        if ('' === $template) {
            return $response; //@codeCoverageIgnore
        }

        /** @var Engine $engine */
        $engine = app(Engine::class);

        $response
            ->getBody()
            ->write(
                $engine->render($template, $args),
            );

        return $response;
    }
}
