<?php

declare(strict_types=1);

namespace App\Core;

use DI\Container as DIContainer;
use DI\ContainerBuilder;

/** @codeCoverageIgnore */
final class Container
{
    private function __construct()
    {
    }

    public static ?DIContainer $container = null;

    public static function getInstance(): DIContainer
    {
        if (null === self::$container) {
            $container = (new ContainerBuilder())
                ->addDefinitions(__DIR__ . '/../../config/di-container.php')
                ->useAnnotations(false);

            self::$container = $container->build();
        }

        return self::$container;
    }
}
