<?php

declare(strict_types=1);

namespace App\Core;

use App\Enums\Environment;
use App\Enums\FilesystemDrive;
use Dotenv\Dotenv;

final class EnvironmentLoader
{
    public function __construct(private readonly string $rootDir)
    {
    }

    public function load(Environment $environment = null): void
    {
        $filename = $this->determineFileName($environment);

        if (!file_exists($this->rootDir.'/'.$filename)) {
            throw new \RuntimeException(sprintf('A %s file is necessary to run this application', $filename));
        }

        $dotenv = Dotenv::createImmutable($this->rootDir, $filename);

        $dotenv->load();

        $dotenv
            ->required('APP_NAME');

        $dotenv
            ->required('ENVIRONMENT')
            ->allowedValues(array_map(static fn (Environment $environment): string => $environment->value, Environment::cases()));

        $dotenv
            ->required('FILESYSTEM_DRIVE')
            ->allowedValues(array_map(static fn (FilesystemDrive $filesystemDrive): string => $filesystemDrive->value, FilesystemDrive::cases()));

        $dotenv
            ->required('USERNAME');

        $dotenv
            ->required('PASSWORD');

        $dotenv
            ->required('APP_KEY');
    }

    private function determineFileName(Environment $environment = null): string
    {
        return match ($environment) {
            Environment::Testing => '.env.testing',
            default => '.env',
        };
    }
}
