<?php

declare(strict_types=1);

namespace App\Console\Commands\Utility;

use Psy\Shell as BaseShell;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/** @codeCoverageIgnore */
final class ShellCommand extends Command
{
    protected static $defaultName = 'utility:shell';

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var BaseShell $shell */
        $shell = app(BaseShell::class);
        $shell->setOutput($output);

        return $shell->run($input);
    }

    protected function configure(): void
    {
        $this->setDescription('Interactive Psy shell');
    }
}
