<?php

declare(strict_types=1);

namespace App\Filesystem\Factories;

use App\Enums\FilesystemFolder;
use Carbon\Carbon;
use League\Flysystem\FileAttributes;
use League\Flysystem\Filesystem;
use League\Flysystem\UnableToGenerateTemporaryUrl;

final class FileAttributesUrlFactory
{
    public function __construct(
        private readonly Filesystem $filesystem,
    ) {
    }

    public function create(FileAttributes $fileAttributes, FilesystemFolder $folder): string
    {
        $path = $folder->value . '/' . basename($fileAttributes->path());

        try {
            return $this->filesystem->temporaryUrl($path, Carbon::now()->addDay());
        } catch (UnableToGenerateTemporaryUrl) {
            return $this->filesystem->publicUrl($path);
        }
    }
}
