<?php

declare(strict_types=1);

namespace App\Filesystem\Factories;

use App\Filesystem\Generators\LocalFilesystemPublicUrlGenerator;
use App\Filesystem\Traits\DeterminesAdapterClassNames;
use League\Flysystem\InMemory\InMemoryFilesystemAdapter;
use League\Flysystem\UrlGeneration\PublicUrlGenerator;

final class PublicUrlGeneratorFactory
{
    use DeterminesAdapterClassNames;

    public function create(): ?PublicUrlGenerator
    {
        return match ($this->determineAdapterClassName()) {
            InMemoryFilesystemAdapter::class => new LocalFilesystemPublicUrlGenerator(),
            default => null,
        };
    }
}
