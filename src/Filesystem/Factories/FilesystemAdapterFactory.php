<?php

declare(strict_types=1);

namespace App\Filesystem\Factories;

use App\Filesystem\Traits\DeterminesAdapterClassNames;
use DI\Container;
use League\Flysystem\FilesystemAdapter;

final class FilesystemAdapterFactory
{
    use DeterminesAdapterClassNames;

    public function __construct(
        private readonly Container $container,
    ) {
    }

    public function create(): FilesystemAdapter
    {
        return $this->container->get($this->determineAdapterClassName());
    }
}
