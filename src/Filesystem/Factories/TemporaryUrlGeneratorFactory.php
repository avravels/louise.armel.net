<?php

declare(strict_types=1);

namespace App\Filesystem\Factories;

use App\Filesystem\Generators\LocalFilesystemTemporaryUrlGenerator;
use App\Filesystem\Traits\DeterminesAdapterClassNames;
use League\Flysystem\Local\LocalFilesystemAdapter;
use League\Flysystem\UrlGeneration\TemporaryUrlGenerator;

final class TemporaryUrlGeneratorFactory
{
    use DeterminesAdapterClassNames;

    public function create(): ?TemporaryUrlGenerator
    {
        return match ($this->determineAdapterClassName()) {
            LocalFilesystemAdapter::class => app(LocalFilesystemTemporaryUrlGenerator::class),
            default => null,
        };
    }
}
