<?php

declare(strict_types=1);

namespace App\Filesystem\Traits;

use App\Enums\FilesystemDrive;
use League\Flysystem\AwsS3V3\AwsS3V3Adapter;
use League\Flysystem\FilesystemAdapter;
use League\Flysystem\InMemory\InMemoryFilesystemAdapter;
use League\Flysystem\Local\LocalFilesystemAdapter;

trait DeterminesAdapterClassNames
{
    /** @return class-string<FilesystemAdapter> */
    private function determineAdapterClassName(): string
    {
        return match(FilesystemDrive::current()) {
            FilesystemDrive::Local => LocalFilesystemAdapter::class,
            FilesystemDrive::S3 => AwsS3V3Adapter::class,
            FilesystemDrive::Memory => InMemoryFilesystemAdapter::class,
        };
    }
}
