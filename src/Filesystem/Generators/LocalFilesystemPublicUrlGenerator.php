<?php

declare(strict_types=1);

namespace App\Filesystem\Generators;

use League\Flysystem\Config;
use League\Flysystem\UrlGeneration\PublicUrlGenerator;

final class LocalFilesystemPublicUrlGenerator implements PublicUrlGenerator
{
    public function publicUrl(string $path, Config $config): string
    {
        return $path;
    }
}
