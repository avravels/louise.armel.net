<?php

declare(strict_types=1);

namespace App\Filesystem\Generators;

use App\Crypt\Hasher;
use DateTimeInterface;
use League\Flysystem\Config;
use League\Flysystem\UrlGeneration\TemporaryUrlGenerator;

final class LocalFilesystemTemporaryUrlGenerator implements TemporaryUrlGenerator
{
    public function __construct(
        private readonly Hasher $hasher,
    ) {
    }

    public function temporaryUrl(string $path, DateTimeInterface $expiresAt, Config $config): string
    {
        return $path . '?' . http_build_query([
            'expires_at' => $timestamp = $expiresAt->format('U'),
            'signature' => $this->hasher->hash($timestamp),
        ]);
    }
}
