<?php

declare(strict_types=1);

namespace App\Crypt;

final class Hasher
{
    public function __construct(
        #[\SensitiveParameter]
        private readonly string $salt,
    ) {
    }

    public function hash(string $string): string
    {
        return md5($this->salt . $string);
    }
}
