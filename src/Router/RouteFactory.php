<?php

declare(strict_types=1);

namespace App\Router;

use App\Http\Routes;
use League\Route\Router;
use League\Route\Strategy\ApplicationStrategy;
use Psr\Container\ContainerInterface;

final class RouteFactory
{
    public static function build(ContainerInterface $container): Router
    {
        $strategy = new ApplicationStrategy();
        $strategy->setContainer($container);

        $router = new Router();
        $router->setStrategy($strategy);

        Routes::routes($router);

        return $router;
    }
}
