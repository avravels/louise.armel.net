<?php

declare(strict_types=1);

namespace App\Http;

use App\Http\Controller\AssetsController;
use App\Http\Controller\PhotoController;
use App\Http\Middleware\BasicAuthMiddleware;
use App\Http\Middleware\TemporaryUrlMiddleware;
use League\Route\RouteGroup;
use League\Route\Router;

final class Routes
{
    public static function routes(Router $route): void
    {
        $route->get('/', [PhotoController::class, 'home'])
            ->middleware(app(BasicAuthMiddleware::class));

        $route
            ->group('', function (RouteGroup $route): void {
                $route->get('thumbnails/{photo}', [PhotoController::class, 'showThumbnail']);
                $route->get('photos/{photo}', [PhotoController::class, 'showPhoto']);
                $route->map('GET', 'css/{path:.*}', AssetsController::class);
            })
            ->middleware(app(TemporaryUrlMiddleware::class));
    }
}
