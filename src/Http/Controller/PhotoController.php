<?php

declare(strict_types=1);

namespace App\Http\Controller;

use App\Filesystem\Factories\FileAttributesUrlFactory;
use App\Http\Response\View;
use App\Repository\PhotoNotFoundException;
use App\Repository\PhotoRepositoryInterface;
use Intervention\Image\ImageManager;
use League\Flysystem\Filesystem;
use League\Route\Http\Exception\NotFoundException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class PhotoController
{
    public function __construct(
        private readonly PhotoRepositoryInterface $photoRepository,
        private readonly FileAttributesUrlFactory $urlFactory,
        private readonly Filesystem $filesystem,
        private readonly ImageManager $imageManager,
    ) {
    }

    public function home(): View
    {
        return view('home', [
            'urlFactory' => $this->urlFactory,
            'photos' => $this->photoRepository->getPhotos(),
        ]);
    }

    /** @param string[] $args */
    public function showThumbnail(ServerRequestInterface $request, array $args = []): ResponseInterface
    {
        try {
            return $this->imageManager
                ->make(
                    $this->filesystem->readStream(
                        $this->photoRepository->getFileAttributesByPath('thumbnails/'. urldecode($args['photo']))->path(),
                    ),
                )
                ->psrResponse('jpg');
        } catch (PhotoNotFoundException) {
            try {
                $thumbnail = $this->imageManager
                    ->make(
                        $this->filesystem->readStream(
                            $this->photoRepository->getFileAttributesByPath('photos/' . urldecode($args['photo']))->path(),
                        ),
                    )
                    ->widen(300);

                $this->filesystem->write('thumbnails/'. urldecode($args['photo']), $thumbnail->stream('jpg', 100)->getContents());

                return $thumbnail->psrResponse('jpg');
            } catch (PhotoNotFoundException) {
                throw new NotFoundException();
            }
        }
    }

    /** @param string[] $args */
    public function showPhoto(ServerRequestInterface $request, array $args = []): ResponseInterface
    {
        try {
            return $this->imageManager
                ->make(
                    $this->filesystem->readStream(
                        $this->photoRepository->getFileAttributesByPath('photos/'. urldecode($args['photo']))->path(),
                    ),
                )
                ->psrResponse('jpg');
        } catch (PhotoNotFoundException) {
            throw new NotFoundException();
        }
    }
}
