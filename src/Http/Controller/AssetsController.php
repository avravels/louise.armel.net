<?php

declare(strict_types=1);

namespace App\Http\Controller;

use League\Flysystem\Filesystem;
use League\Route\Http\Exception\NotFoundException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class AssetsController
{
    public function __construct(
        private readonly Filesystem $filesystem,
    ) {
    }

    /** @param array<string, mixed> $args */
    public function __invoke(ServerRequestInterface $request, array $args = []): ResponseInterface
    {
        if  (
            '' === $args['path']
            || !$this->filesystem->fileExists('css/'. $args['path'])
        ) {
            throw new NotFoundException();
        }

        $response = response()
            ->withHeader('Content-Type', $this->filesystem->mimeType('css/'.$args['path']));

        $response
            ->getBody()
            ->write(
                $this->filesystem->read('css/'.$args['path']),
            );

        return $response;
    }
}
