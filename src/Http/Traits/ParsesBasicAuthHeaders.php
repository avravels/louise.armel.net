<?php

declare(strict_types=1);

namespace App\Http\Traits;

use Psr\Http\Message\RequestInterface;
use Webmozart\Assert\Assert;

trait ParsesBasicAuthHeaders
{
    /** @return array<int, string|null> */
    private function parseBasicAuthHeader(RequestInterface $request): array
    {
        $headerLine = $request->getHeaderLine('Authorization');

        if (!str_starts_with($headerLine, 'Basic')) {
            return [
                null,
                null,
            ];
        }

        /** @var string $headerLine */
        $headerLine = base64_decode(substr($headerLine, 6), true);

        Assert::string($headerLine);

        $headerLine = explode(':', $headerLine, 2);

        return [
            $headerLine[0],
            $headerLine[1] ?? null,
        ];
    }
}
