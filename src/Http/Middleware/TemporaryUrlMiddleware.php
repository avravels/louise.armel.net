<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Crypt\Hasher;
use Carbon\Carbon;
use League\Route\Http\Exception\ForbiddenException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class TemporaryUrlMiddleware implements MiddlewareInterface
{
    public function __construct(
        private readonly Hasher $hasher,
    ) {
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $params = $request->getQueryParams();

        if (!isset($params['expires_at'], $params['signature'])) {
            throw new ForbiddenException();
        }

        if (!hash_equals($this->hasher->hash($params['expires_at']), $params['signature'])) {
            throw new ForbiddenException('Signature does not match');
        }

        if (Carbon::now() >= Carbon::createFromTimestamp($params['expires_at'])) {
            throw new ForbiddenException('Expired timestamp');
        }

        return $handler->handle($request);
    }
}
