<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Authentication\UserAuthenticator;
use App\Http\Traits\ParsesBasicAuthHeaders;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class BasicAuthMiddleware implements MiddlewareInterface
{
    use ParsesBasicAuthHeaders;

    public function __construct(
        private readonly UserAuthenticator $userAuthenticator,
    ) {
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        [$username, $password] = $this->parseBasicAuthHeader($request);

        if (
            null === $username ||
            null === $password ||
            false === $this->userAuthenticator->checkCredentials($username, $password)
        ) {
            return response()
                ->withStatus(401)
                ->withHeader('WWW-Authenticate', 'Basic realm="Login"');
        }

        return $handler->handle($request);
    }
}
