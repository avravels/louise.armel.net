<?php

declare(strict_types=1);

namespace App\Http;

use App\Core\EnvironmentLoader;
use App\Enums\Environment;
use App\Router\RouteFactory;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Psr\Container\ContainerInterface;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

/** @codeCoverageIgnore  */

final class Kernel
{
    public function __construct(private readonly ContainerInterface $container)
    {
    }

    public function run(): void
    {
        $this->container->get(EnvironmentLoader::class)->load();

        if (Environment::Production === app('environment')) {
            set_exception_handler($this->container->get(ExceptionHandler::class)->handle(...));
        } else {
            $whoops = new Run();
            $whoops->pushHandler(new PrettyPageHandler());
            $whoops->register();
        }

        $route = RouteFactory::build($this->container);
        $response = $route->dispatch($this->container->get('request'));
        $this->container->get(SapiEmitter::class)->emit($response);
    }
}
