<?php

declare(strict_types=1);

namespace App\Http;

use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use League\Route\Http\Exception;

/** @codeCoverageIgnore */
final class ExceptionHandler
{
    public function __construct(private readonly SapiEmitter $emitter)
    {
    }

    public function handle(\Throwable $error): void
    {
        $this
            ->emitter
            ->emit(
                view(
                    'error',
                    [
                        'status' => $statusCode = $error instanceof Exception ? $error->getStatusCode() : 500,
                        'message' => $error instanceof Exception ? $error->getMessage() : 'Internal Server Error',
                    ],
                )
                ->withStatus($statusCode),
            );
    }
}
