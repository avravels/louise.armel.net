<?php

declare(strict_types=1);

namespace App\Authentication;

final class UserAuthenticator
{
    public function __construct(
        private readonly string $username,
        #[\SensitiveParameter]
        private readonly string $password,
    ) {
    }

    public function checkCredentials(
        string $username,
        #[\SensitiveParameter]
        string $password,
    ): bool {
        return hash_equals($this->username . $this->password, $username . $password);
    }
}
