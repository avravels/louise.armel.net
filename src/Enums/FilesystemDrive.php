<?php

declare(strict_types=1);

namespace App\Enums;

enum FilesystemDrive: string
{
    case Local = 'local';
    case S3 = 'S3';
    case Memory = 'memory';

    public static function current(): self
    {
        return self::from($_ENV['FILESYSTEM_DRIVE']);
    }
}
