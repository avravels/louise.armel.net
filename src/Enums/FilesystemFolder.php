<?php

declare(strict_types=1);

namespace App\Enums;

enum FilesystemFolder: string
{
    case Thumbnails = 'thumbnails';
    case Photos = 'photos';
    case Css = 'css';
}
