<?php

declare(strict_types=1);

namespace App\Enums;

/** @codeCoverageIgnore */
enum Environment: string
{
    case Development = 'development';
    case Production = 'production';
    case Testing = 'testing';

    public static function current(): self
    {
        return self::from($_ENV['ENVIRONMENT']);
    }
}
