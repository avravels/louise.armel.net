<?php

declare(strict_types=1);

namespace App\Repository;

use League\Flysystem\FileAttributes;
use League\Flysystem\Filesystem;
use League\Flysystem\StorageAttributes;

final class PhotoRepository implements PhotoRepositoryInterface
{
    public function __construct(
        private readonly Filesystem $filesystem,
    ) {
    }

    /** @return iterable<FileAttributes> */
    public function getPhotos(): iterable
    {
        foreach ($this->filesystem->listContents('photos') as $item) {
            if (
                $item instanceof FileAttributes
                && $item->isFile()
                && !str_starts_with(basename($item->path()), '.')
            ) {
                yield $item;
                unset($item);
            }
        }
    }

    public function getFileAttributesByPath(string $path): FileAttributes
    {
        if (!$this->filesystem->fileExists($path)) {
            throw new PhotoNotFoundException();
        }

        return FileAttributes::fromArray([
            StorageAttributes::ATTRIBUTE_PATH => $path,
        ]);
    }
}
