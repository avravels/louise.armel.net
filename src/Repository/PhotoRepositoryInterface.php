<?php

declare(strict_types=1);

namespace App\Repository;

use League\Flysystem\FileAttributes;

interface PhotoRepositoryInterface
{
    /** @return iterable<FileAttributes> */
    public function getPhotos(): iterable;

    public function getFileAttributesByPath(string $path): FileAttributes;
}
